import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import React from 'react'
import { useRouter } from 'next/router'
import PropTypes from "prop-types";
export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      data: []
    }
  }
  async componentDidMount() {
    const res = await fetch('https://s9d5.tx11.idrivee2-3.com/ucapanpw/test.json')
    const posts = await res.json()
    const pathname = window.location.pathname.substring(1);
    let data = posts.find(v => {
      return v.id == pathname
    })
    this.setState({data:data})
  }

  render() {
    const { match, location, history } = this.props;
    let{data}=this.state
    return (
      <>
        <iframe
          src={data.redirect}
          frameBorder="0"
          style={{
            overflow: "hidden",
            overflowX: "hidden",
            overflowY: "hidden",
            height: "100%",
            width: "100%",
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
          }}

          height="100%" width="100%"></iframe>
      </>
    )

  }
}
